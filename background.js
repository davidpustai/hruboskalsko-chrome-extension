function updateIcon() {
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "http://hruboskalsko.cz/", true);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			var div = document.createElement('div');
			div.innerHTML = xhr.responseText;

			var podminky = div.getElementsByTagName('div')[5].getElementsByTagName('strong')[0];
			result = podminky.innerHTML.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');
			div=null;
		}
	};
	xhr.send();

	if (result=='Skály velmi mokré,<br>lezení zakázáno!')
		chrome.browserAction.setIcon({path:"red.png"});
	else if (result=='Skály mokré,<br>lezení zakázáno!')
		chrome.browserAction.setIcon({path:"orange.png"});
	else if (result=='Skály místy mokré,<br>lezení podmíněně<br>povoleno!')
		chrome.browserAction.setIcon({path:"yellow.png"});
	else if (result=='Skály suché,<br>lezení povoleno!')
		chrome.browserAction.setIcon({path:"green.png"});
	else if (result=='Aktuální lezecké<br>podmínky pro dnešní<br>den ještě nebyly<br>stanoveny!')
		chrome.browserAction.setIcon({path:"white.png"});
	else
		chrome.browserAction.setIcon({path:"grey.png"});
}

chrome.browserAction.onClicked.addListener(updateIcon);
updateIcon();